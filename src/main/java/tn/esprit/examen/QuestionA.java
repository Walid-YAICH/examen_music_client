package tn.esprit.examen;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entity.DJ;
import tn.esprit.service.DjServiceRemote;

public class QuestionA {
	public static void main(String[] args) throws NamingException {
		String jndiName="examen_music-ear/examen_music-ejb/DJService!tn.esprit.service.DjServiceRemote";
		Context context=new InitialContext();
		DjServiceRemote proxy= (DjServiceRemote) context.lookup(jndiName);

		proxy.ajouter_DJ(new DJ(1L, "david.guetta@dj.com", "p", "David guetta", "UK"));
		proxy.ajouter_DJ(new DJ(35L, "tiesto@dj.com", "&", "Tiesto", "US"));
	}
}
