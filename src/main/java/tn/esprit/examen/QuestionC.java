package tn.esprit.examen;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entity.DJ;
import tn.esprit.entity.Playlist;
import tn.esprit.service.DjServiceRemote;

public class QuestionC {
	public static void main(String[] args) throws NamingException {
		String jndiName="examen_music-ear/examen_music-ejb/DJService!tn.esprit.service.DjServiceRemote";
		Context context=new InitialContext();
		DjServiceRemote proxy= (DjServiceRemote) context.lookup(jndiName);

		proxy.affecter_Playlist_A_DJ(6L, 1L);
		proxy.affecter_Playlist_A_DJ(7L, 1L);
	}
}
