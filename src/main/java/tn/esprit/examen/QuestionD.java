package tn.esprit.examen;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entity.Song;
import tn.esprit.entity.Style;
import tn.esprit.service.DjServiceRemote;

public class QuestionD {
	public static void main(String[] args) throws NamingException {
		String jndiName="examen_music-ear/examen_music-ejb/DJService!tn.esprit.service.DjServiceRemote";
		Context context=new InitialContext();
		DjServiceRemote proxy= (DjServiceRemote) context.lookup(jndiName);

		proxy.ajouter_Song_et_affecter_a_Playlist(
				new Song(2L, "So far away", Integer.valueOf(400), Style.POP), 6L);
		
		proxy.ajouter_Song_et_affecter_a_Playlist(
				new Song(5L, "Memories", Integer.valueOf(500), Style.POP), 6L);
		
		proxy.ajouter_Song_et_affecter_a_Playlist(
				new Song(15L, "The world is mine", Integer.valueOf(300), Style.POP), 7L);
	}
}
