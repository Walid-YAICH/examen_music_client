package tn.esprit.examen;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.service.DjServiceRemote;

public class QuestionF {
	public static void main(String[] args) throws NamingException {
		String jndiName="examen_music-ear/examen_music-ejb/DJService!tn.esprit.service.DjServiceRemote";
		Context context=new InitialContext();
		DjServiceRemote proxy= (DjServiceRemote) context.lookup(jndiName);

		System.out.println(proxy.nombre_total_songs());
		System.out.println(proxy.getDJByEmailAndPassword("david.guetta@dj.com", "p").getName());
	}
}
