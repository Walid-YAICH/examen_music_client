package tn.esprit.examen;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entity.Playlist;
import tn.esprit.service.DjServiceRemote;

public class QuestionE {
	public static void main(String[] args) throws NamingException {
		String jndiName="examen_music-ear/examen_music-ejb/DJService!tn.esprit.service.DjServiceRemote";
		Context context=new InitialContext();
		DjServiceRemote proxy= (DjServiceRemote) context.lookup(jndiName);

		List<Playlist> playlists = proxy.recuperer_Playlist_Par_DJ(1L);
		for(Playlist playlist : playlists){
			System.out.println("playlist name : " + playlist.getName());
		}
		System.out.println("playlists.size() : " + playlists.size());
		
		//L'explication du doublant : 
		// https://stackoverflow.com/questions/20749806/duplicates-in-onetomany-annotated-list
	}
}
