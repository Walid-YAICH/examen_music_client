package tn.esprit.examen;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entity.DJ;
import tn.esprit.entity.Playlist;
import tn.esprit.service.DjServiceRemote;

public class QuestionB {
	public static void main(String[] args) throws NamingException {
		String jndiName="examen_music-ear/examen_music-ejb/DJService!tn.esprit.service.DjServiceRemote";
		Context context=new InitialContext();
		DjServiceRemote proxy= (DjServiceRemote) context.lookup(jndiName);

		proxy.ajouter_Playlist(new Playlist(6L, "Scream", 900));
		proxy.ajouter_Playlist(new Playlist(7L, "Show Me", 300));
	}
}
